<!-- products section starts  -->

<section class="products" id="products">

    <h1 class="heading"> Produtos <span>Mais recentes</span> </h1>
    
    <div class="box-container">
    <?php 


$query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 4 ");
$res = $query->fetchAll(PDO::FETCH_ASSOC);

for ($i=0; $i < count($res); $i++) { 
foreach ($res[$i] as $key => $value) {
}

$nome = $res[$i]['nome'];
$valor = $res[$i]['valor'];
$nome_url = $res[$i]['nome_url'];
$imagem = $res[$i]['imagem'];
$promocao = $res[$i]['promocao'];
$id = $res[$i]['id'];

$valor = number_format($valor, 2, ',', '.');

?>
        <div class="box">
            <div class="icons">
                <a href="#" class="fas fa-heart"></a>
                <a href="#" class="fas fa-share"></a>
                <a href="#" class="fas fa-eye"></a>
            </div>
            <img src="img/produtos/<?php echo $imagem ?>" alt="Imagem1">
            <div class="content">
                <h3><?php echo $nome ?></h3>
                
                <div class="price">
                   <?php  
                if($promocao == 'Sim'){
                    $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id'  ");
                    $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                    $valor_promo = $resp[0]['valor'];
                    $desconto = $resp[0]['desconto'];
                    $valor_promo = number_format($valor_promo, 2, ',', '.');


                ?>
                 <span>R$ <?php echo $valor_promo ?></span> 
                <?php }else ?>
                    R$ <?php echo $valor ?> 
                 </div>
                
                <a href="" onclick="carrinhoModal('<?php echo $id ?>','Não')" class="btn">Comprar</a>
            </div>
        </div>
       
        

        

        <?php }  ?>

    </div>
   
</section>


<section class="products-slides" id="products">

        <h1 class="heading">Produtos <span>Destaques</span> </h1>

        <div class="swiper-container product-slider gap">
            <div class="swiper-wrapper">
                <?php 
                    $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 16 ");
                    $res = $query->fetchAll(PDO::FETCH_ASSOC);

                    for ($i=0; $i < count($res); $i++) { 
                    foreach ($res[$i] as $key => $value) {
                    }

                    $nome = $res[$i]['nome'];
                    $valor = $res[$i]['valor'];
                    $nome_url = $res[$i]['nome_url'];
                    $imagem = $res[$i]['imagem'];
                    $promocao = $res[$i]['promocao'];
                    $id = $res[$i]['id'];

                    $valor = number_format($valor, 2, ',', '.');
                ?>
                <div class="swiper-slide col-lg-3 col-md-4 col-sm-6">
                    <div class="slide">
                        <div class="icons">
                            <a href="#" class="fas fa-heart"></a>
                            <a href="#" class="fas fa-share"></a>
                            <a href="#" class="fas fa-eye"></a>
                        </div>
                        <div class="image">                          
                        <img src="img/produtos/<?php echo $imagem ?>" alt="Imagem1">
                        </div>
                        <div class="content">
                           
                            <h3><?php echo $nome ?></h3>
                            <div class="price">
                                <?php 
                                    if($promocao == 'Sim'){
                                        $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id'  ");
                                        $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                                        $valor_promo = $resp[0]['valor'];
                                        $desconto = $resp[0]['desconto'];
                                        $valor_promo = number_format($valor_promo, 2, ',', '.');
                    
                    
                                    
                                ?>
                                <span>R$ <?php echo $valor_promo ?></span>
                                <?php }?> 
                                R$ <?php echo $valor ?>
                            </div>
                            <a href="#" class="btn">Comprar</a>
                        </div>
                    </div>
                </div>
            <?php }  ?> 
               
                
                
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>

        

    </section>

<!-- products section ends -->