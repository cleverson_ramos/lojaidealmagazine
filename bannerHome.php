
<?php 

$query = $pdo->query("SELECT * FROM banners_topo where ativo = 'Sim' order by order_banner ASC ");
$res = $query->fetchAll(PDO::FETCH_ASSOC);

      

       
?>

<div class="container">
    <section class="home">

        <div class="swiper home-slider">
                
            <div class="swiper-wrapper">
                <?php

                    for ($i=0; $i < count($res); $i++) { 
                        foreach ($res[$i] as $key => $value) {
                        }

                        $nome = $res[$i]['nome_banner'];
                        $imagem = $res[$i]['imagem'];
                        $texto_acao_banner = $res[$i]['texto_acao_banner'];
                        $titulo_banner = $res[$i]['titulo_banner'];
                        $link = $res[$i]['link'];
                        $ativo = $res[$i]['ativo'];

                    ?>
                    <div class="swiper-slide slide">
                
                        <div class="image">
                            <img src="img/banner/home/<?php echo $imagem ?>" alt="">
                            
                        </div>
                    
                        <div class="content">
                        
                            <span><?php echo $texto_acao_banner; ?></span>
                            <h3><?php echo  $titulo_banner; ?></h3>
                        
                            <a href="<?php echo $link; ?>" class="btn">Comprar</a>
                        </div>

                    </div>
            
                    <?php } ?>

            
            
            </div>
        
        
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>

        </div>

    </section>
</div>