<div class="container">
        <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                    
                        <h2>Produtos Destaques </h2>
                        <a class="text-dark" href="produtos.php"><span ><small>Ver + Produtos</small></span></a>
                    </div>
                
                </div>
            </div>
            <div class="row featured__filter">

                
                    <?php 
                    $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 4 ");
                    $res = $query->fetchAll(PDO::FETCH_ASSOC);

                    for ($i=0; $i < count($res); $i++) { 
                    foreach ($res[$i] as $key => $value) {
                    }

                    $nome = $res[$i]['nome'];
                    $valor = $res[$i]['valor'];
                    $nome_url = $res[$i]['nome_url'];
                    $imagem = $res[$i]['imagem'];
                    $promocao = $res[$i]['promocao'];
                    $id = $res[$i]['id'];

                    $valor = number_format($valor, 2, ',', '.');

                    if($promocao == 'Sim'){
                        $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                        $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                        $valor_promo = $resp[0]['valor'];
                        $desconto = $resp[0]['desconto'];
                        $valor_promo = number_format($valor_promo, 2, ',', '.');

                        ?>
                   
                    <div class="col-lg-3 col-md-4 col-sm-6 mix. <? echo $res[$i]; ?>. fresh-mea a">
                        <div class="product__discount__item">
                            <div class="product__discount__item__pic set-bg"
                            data-setbg="img/produtos/<?php echo $imagem ?>">
                            <div class="product__discount__percent">
                                -<?php echo $desconto ?>%
                            </div>
                            <ul class="product__item__pic__hover">
                                <li><a href="produto-<?php echo $nome_url ?>"><i class="Teste 1fa fa-eye"></i></a></li>
                                <li><a href="" onclick="carrinhoModal('<?php echo $id ?>','Não')"><i class="fa fa-shopping-cart"></i></a>
                            </ul>
                        </div>
                        <div class="icons">
                            <a class="fa fa-whatsapp" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp_link ?>&text=Pode me %ajudar?&source=&data=&app_absent=%22%3E"></a>
                            
                        </div>
                    <div class="product__discount__item__text">

                        <h5><a href="produto-<?php echo $nome_url ?>"><?php echo $nome ?></a></h5>
                        <div class="product__item__price">R$ <?php echo $valor_promo ?> <span>R$ <?php echo $valor ?></span></div>
                    </div>
                    </div>
                </div>

                <?php }else{ ?>


                <div class="col-lg-3 col-md-4 col-sm-6 mix <?php echo $nome; ?> fresh-meat">
                   
                    <div class="featured__item">
                        <div class="icons">
                            <a class="fa fa-whatsapp" target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $whatsapp_link ?>&text=<?php echo $nome ?> ainda está disponivel? Produto: <?php echo $nome_url ?>?&source=&data=&app_absent=%22%3E"></a>
                            
                        </div>
                        <a href="produto-<?php echo $nome_url ?>">
                            <div class="featured__item__pic set-bg" data-setbg="img/produtos/<?php echo $imagem ?>">
                                
                            </div>
                        </a>
                        <div class="featured__item__text">
                            <a href="produto-<?php echo $nome_url ?>"><h6><?php echo $nome ?></h6>
                                <h5>R$ <?php echo $valor ?></h5>
                            </a>
                            
                            <a href="" onclick="carrinhoModal('<?php echo $id ?>','Não')" class="btn">Comprar</a>
                        </div>
                    </div>
                
                </div>

                    <?php } } ?>

                
            </div>

        </div>
    </section>


</div>
