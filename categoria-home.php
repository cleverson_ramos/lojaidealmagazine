



<!-- Categories Section Begin -->
<section class="categories">
  <div class="container">
    <div class="row">
      <div class="categories__slider owl-carousel">

       <?php 
       $query = $pdo->query("SELECT * FROM categorias order by id ");
       $res = $query->fetchAll(PDO::FETCH_ASSOC);

       for ($i=0; $i < count($res); $i++) { 
        foreach ($res[$i] as $key => $value) {
        }

        $nome = $res[$i]['nome'];
        $imagem = $res[$i]['imagem'];
        $nome_url = $res[$i]['nome_url'];

        ?>

        <div class="col-lg-3">
          <div class="categories__item set-bg" data-setbg="img/categorias/<?php echo $imagem ?>">
            <h5><a href="sub-categoria-de-<?php echo $nome_url ?>"><?php echo $nome ?></a></h5>
          </div>
        </div>

      <?php } ?>

    </div>
  </div>
</div>
</section>
<!-- Categories Section End -->