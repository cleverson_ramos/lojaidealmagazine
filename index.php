<?php
@session_start();
require_once("cabecalho.php");
require_once("conexao.php");
require_once("bannerHome.php");
require_once("produtos-home.php");
require_once("categoria-home.php");
require_once("destaque-home.php");
?>




<!-- Featured Section Begin -->
<section class="featured spad">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="section-title">
         <a class="text-dark" href="produtos.php"><span ><small>Ver + Produtos</small></span></a>
         <h2>Produtos Destaques </h2>

       </div>
       <div class="featured__controls">
        <ul>
         <?php 
         $query = $pdo->query("SELECT * FROM sub_categorias order by id limit 5 ");
         $res = $query->fetchAll(PDO::FETCH_ASSOC);

         for ($i=0; $i < count($res); $i++) { 
          foreach ($res[$i] as $key => $value) {
          }

          $nome = $res[$i]['nome'];

          $nome_url = $res[$i]['nome_url'];

          ?>
          <li><a href="produtos-<?php echo $nome_url ?>" class="text-dark"><?php echo $nome ?></a></li>
        <?php } ?>

      </ul>
    </div>
  </div>
</div>
<div class="row featured__filter">


 <?php 
 $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 8 ");
 $res = $query->fetchAll(PDO::FETCH_ASSOC);

 for ($i=0; $i < count($res); $i++) { 
  foreach ($res[$i] as $key => $value) {
  }

  $nome = $res[$i]['nome'];
  $valor = $res[$i]['valor'];
  $nome_url = $res[$i]['nome_url'];
  $imagem = $res[$i]['imagem'];
  $promocao = $res[$i]['promocao'];
  $id = $res[$i]['id'];

  $valor = number_format($valor, 2, ',', '.');

  if($promocao == 'Sim'){
    $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
    $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
    $valor_promo = $resp[0]['valor'];
    $desconto = $resp[0]['desconto'];
    $valor_promo = number_format($valor_promo, 2, ',', '.');

    ?>

    <div class="col-lg-3 col-md-4 col-sm-6 mix sapatos fresh-meat">
      <div class="product__discount__item">
        <div class="product__discount__item__pic set-bg"
        data-setbg="img/produtos/<?php echo $imagem ?>">
        <div class="product__discount__percent">-<?php echo $desconto ?>%</div>
        <ul class="product__item__pic__hover">
         <li><a href="produto-<?php echo $nome_url ?>"><i class="Teste 1fa fa-eye"></i></a></li>


         <li><a href="" onclick="carrinhoModal('<?php echo $id ?>','Não')"><i class="fa fa-shopping-cart"></i></a>


         </ul>
       </div>
       <div class="product__discount__item__text">

        <h5><a href="produto-<?php echo $nome_url ?>"><?php echo $nome ?></a></h5>
        <div class="product__item__price">R$ <?php echo $valor_promo ?> <span>R$ <?php echo $valor ?></span></div>
      </div>
    </div>
  </div>

<?php }else{ ?>


  <div class="col-lg-3 col-md-4 col-sm-6 mix sapatos fresh-meat">
    <div class="featured__item">
      <div class="featured__item__pic set-bg" data-setbg="img/produtos/<?php echo $imagem ?>">
        <ul class="featured__item__pic__hover">
          <li><a href="produto-<?php echo $nome_url ?>"><i class="fa fa-eye"></i></a></li>

          <li><a href="" onclick="carrinhoModal('<?php echo $id ?>','Não')"><i class="fa fa-shopping-cart"></i></a>

          </ul>
        </div>
        <div class="featured__item__text">
          <a href="produto-<?php echo $nome_url ?>"><h6><?php echo $nome ?></h6>
            <h5>R$ <?php echo $valor ?></h5></a>
          </div>
        </div>
      </div>

    <?php } } ?>


  </div>
</div>
</section>
<!-- Featured Section End -->

<!-- Banner Begin -->
<div class="banner">
  <div class="container">
    <div class="row">

     <?php 
     $query = $pdo->query("SELECT * FROM promocao_banner where ativo = 'Sim' order by id desc limit 2 ");
     $res = $query->fetchAll(PDO::FETCH_ASSOC);

     for ($i=0; $i < count($res); $i++) { 
      foreach ($res[$i] as $key => $value) {
      }

      $titulo = $res[$i]['titulo'];
      $link = $res[$i]['link'];

      $imagem = $res[$i]['imagem'];


      ?>

      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="banner__pic">
         <a href="<?php echo $link ?>" title="<?php echo $titulo ?>"> <img src="img/promocoes/<?php echo $imagem ?>" alt="<?php echo $titulo ?>"> </a>
       </div>
     </div>

   <?php } ?>

 </div>
</div>
</div>
<!-- Banner End -->

<!-- Latest Product Section Begin -->
<section class="latest-product spad d-none d-md-block">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <div class="latest-product__text">
          <h4>Produtos Recentes</h4>
          <div class="latest-product__slider owl-carousel">
            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos order by id desc limit 3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }

                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by id desc limit 3,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }


                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>



            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by id desc limit 6,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }
                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                 <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <div class="latest-product__text">
          <h4>Mais Vendidos</h4>
          <div class="latest-product__slider owl-carousel">


            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }
                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 3,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }
                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>



            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM produtos where ativo = 'Sim' and estoque > 0 order by vendas desc limit 6,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];
                $promocao = $res[$i]['promocao'];
                $id = $res[$i]['id'];

                if($promocao == 'Sim'){
                  $queryp = $pdo->query("SELECT * FROM promocoes where id_produto = '$id' ");
                  $resp = $queryp->fetchAll(PDO::FETCH_ASSOC);
                  $valor = $resp[0]['valor'];
                  $valor = number_format($valor, 2, ',', '.');
                }else{
                  $valor = number_format($valor, 2, ',', '.');
                }
                ?>


                <a href="produto-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/produtos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <div class="latest-product__text">
          <h4>Combos Promocionais</h4>
          <div class="latest-product__slider owl-carousel">


            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM combos where ativo = 'Sim' order by id desc limit 3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];

                $valor = number_format($valor, 2, ',', '.');
                ?>


                <a href="combo-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/combos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM combos where ativo = 'Sim' order by id desc limit 3,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];

                $valor = number_format($valor, 2, ',', '.');
                ?>


                <a href="combo-<?php echo $nome_url ?>" class="latest-product__item">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/combos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>



            <div class="latest-prdouct__slider__item">

              <?php 
              $query = $pdo->query("SELECT * FROM combos where ativo = 'Sim' order by id desc limit 6,3 ");
              $res = $query->fetchAll(PDO::FETCH_ASSOC);

              for ($i=0; $i < count($res); $i++) { 
                foreach ($res[$i] as $key => $value) {
                }

                $nome = $res[$i]['nome'];
                $valor = $res[$i]['valor'];
                $nome_url = $res[$i]['nome_url'];
                $imagem = $res[$i]['imagem'];

                $valor = number_format($valor, 2, ',', '.');
                ?>


                <a href="combo-<?php echo $nome_url ?>" class="latest-product__item">
                 <div class="row">
                    <div class="col-md-5">
                      <div class="latest-product__item__pic">
                        <img src="img/combos/<?php echo $imagem ?>" alt="">
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="latest-product__item__text">
                        <h6><?php echo $nome ?></h6>
                        <span>R$ <?php echo $valor ?></span>
                      </div>
                    </div>
                  </div>
                </a>

              <?php } ?>


            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Latest Product Section End -->



<?php
require_once("rodape.php");
require_once("modal-carrinho.php");

?>






