<?php 
$pag = "banners";
require_once("../../conexao.php"); 
@session_start();
    //verificar se o usuário está autenticado
if(@$_SESSION['id_usuario'] == null || @$_SESSION['nivel_usuario'] != 'Admin'){
  echo "<script language='javascript'> window.location='../index.php' </script>";

}

/*
//CLASSE PARA OS ITENS ATIVOS
if(@$_GET['acao'] == $item1){
  $item1ativo = 'active';
}else if(@$_GET['acao'] == $item2){
  $item2ativo = 'active';
}else if(@$_GET['acao'] == $item3){
  $item3ativo = 'active';
}else if(@$_GET['acao'] == $item4){
  $item4ativo = 'active';
}else if(@$_GET['acao'] == $item5){
  $item5ativo = 'active';
}else if(@$_GET['acao'] == $item6){
  $item6ativo = 'active';
}else if(@$_GET['acao'] == $item7){
  $item7ativo = 'active';
}else if(@$_GET['acao'] == $item8){
  $item8ativo = 'active';
}else if(@$_GET['acao'] == $item9){
  $item9ativo = 'active';
}
*/



$agora = date('Y-m-d');
?>

<div class="row mt-4 mb-4">
  <a type="button" class="btn-primary btn-sm ml-3 d-none d-md-block" href="index.php?pag=<?php echo $pag ?>&funcao=novo">Novo Banner</a>
  <a type="button" class="btn-primary btn-sm ml-3 d-block d-sm-none" href="index.php?pag=<?php echo $pag ?>&funcao=novo">+</a>

</div>



<!-- DataTales Example -->
<div class="card shadow mb-4">

  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Situação</th>
            <th>Nome do banner</th>
            <th>Ordem do banner</th>
            <th>Imagem</th>
            <th>Ações</th>
          </tr>
        </thead>

        <tbody>

         <?php 

         $query = $pdo->query("SELECT * FROM banners_topo order by id desc ");
         $res = $query->fetchAll(PDO::FETCH_ASSOC);

         for ($i=0; $i < count($res); $i++) { 
          foreach ($res[$i] as $key => $value) {
          }

          @$nome = $res[$i]['nome_banner'];
          @$order_banner = $res[$i]['order_banner'];
          @$estoque = $res[$i]['estoque'];
          @$sub_cat = $res[$i]['sub_categoria'];
          @$imagem = $res[$i]['imagem'];
          @$ativo = $res[$i]['ativo'];
          $mensagem = "";
          $classe = "";
          if($ativo == "Sim"){
            $classe = "text-success";
            $mensagem = "Ativo";
          }else{
            $classe = "text-danger";
            $mensagem = "Inativo";
          }
          @$promocao = $res[$i]['promocao'];

          @$id = $res[$i]['id'];

         

?>


          <tr>
            <td>
                <i class='fas fa-square <?php echo $classe ?>'></i>

                <a href="index.php?pag=<?php echo $pag ?>&funcao=carac&id=<?php echo $id ?>" class="<?php echo $classe ?>">
                  <?php echo $mensagem ?>
                </a>

              </td>
            <td>

              <a href="index.php?pag=<?php echo $pag ?>&funcao=carac&id=<?php echo $id ?>" class="text-info">
                <?php echo $nome ?>
              </a>

            </td>
            <td> <?php echo $order_banner ?></td>
            <td><img src="../../img/banner/home/<?php echo $imagem ?>" width="50"></td>
           
           
            <td>
             <a href="index.php?pag=<?php echo $pag ?>&funcao=editar&id=<?php echo $id ?>" class='text-primary mr-1' title='Editar Dados'><i class='far fa-edit'></i></a>
             <a href="index.php?pag=<?php echo $pag ?>&funcao=excluir&id=<?php echo $id ?>" class='text-danger mr-1' title='Excluir Registro'><i class='far fa-trash-alt'></i></a>
<!--
  Ativar pelo check box
             <a href="index.php?acao=<?//php// echo $item6 ?>" class="nav-link <?//php// echo //$item6ativo ?>"><i class="nav-icon far fa-check-square"></i></a>

        -->
             
          </td>
        </tr>
      <?php } ?>





    </tbody>
  </table>
</div>
</div>
</div>





<!-- Modal -->
<div class="modal fade" id="modalDados" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php 
        if (@$_GET['funcao'] == 'editar') {
          $titulo = "Editar Banner";
          $id2 = $_GET['id'];

          $query = $pdo->query("SELECT * FROM banners_topo where id = '" . $id2 . "' ");
          $res = $query->fetchAll(PDO::FETCH_ASSOC);

          $nome_banner = $res[0]['nome_banner'];
         
          $texto_acao_banner = $res[0]['texto_acao_banner'];
          $titulo_banner = $res[0]['titulo_banner'];
          $link2 = $res[0]['link'];
          $order_banner = $res[0]['order_banner'];
          $imagem = $res[0]['imagem'];
          $id_banner = $res[0]['id'];
          $ativo2 = $res[0]['ativo'];
          
        } else {
          $titulo = "Inserir Banner";

        }


        ?>

        <h5 class="modal-title" id="exampleModalLabel"><?php echo $titulo ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form" method="POST">
        <div class="modal-body">

            <div class="row">
              
                <div class="col-md-11">
                    <div class="form-group">
                        <label >Nome do banner</label>
                        <input value="<?php echo @$nome_banner ?>" type="text" class="form-control form-control-sm" id="nome_banner" name="nome_banner" placeholder="Nome do banner">
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label >Titulo</label>
                       
                        <textarea maxlength="500" class="form-control form-control-sm" id="titulo_banner" name="titulo_banner" ><?php echo @$titulo_banner ?></textarea>
                    </div>
                </div>
          
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label >Texto Chamativo</label>
                       
                        <textarea maxlength="500" class="form-control form-control-sm" id="texto_acao_banner" name="texto_acao_banner" ><?php echo @$texto_acao_banner ?></textarea>
                    </div>
                </div>
          
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Link do produto</label>
                        <input value="<?php echo @$link2 ?>" type="text" class="form-control form-control-sm" id="link" name="link" placeholder="Ex: https://www.idealmagazine/produto-vestidos">
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Ordem do banner</label>
                        <input value="<?php echo @$order_banner ?>" type="text" class="form-control form-control-sm" id="order_banner" name="order_banner" placeholder="Ordem do banner">
                    </div>
                    
                </div>
            </div>

            


      <div class="row">
      

        <div class="col-md-4">
          <div class="form-group">
            <label >Imagem do banner</label>
            <input type="file" value="<?php echo @$imagem ?>"  class="form-control-file" id="imagem" name="imagem" onChange="carregarImg();">
          </div>

          <?php if(@$imagem != ""){ ?>
          
          <?php  }else{ ?>
            <img src="../../img/produtos/sem-foto.jpg" width="100" height="100" id="target">
            
            <?php } ?>
        </div>
        <div class="col-md-3">

          <div class="form-group">
            <label >Ativo</label>
            <select class="form-control form-control-sm" name="ativo" id="ativo">
              <?php 
              if (@$_GET['funcao'] == 'editar') {

                echo "<option value='".$ativo2."' >" . $ativo2 . "</option>";
              }


              if(@$ativo2 != "Sim"){
                echo "<option value='Sim'>Sim</option>"; 
              }

              if(@$ativo2 != "Não"){
                echo "<option value='Não'>Não</option>"; 
              }

              ?>
            </select>               
          </div>
          </div>
      </div>
      






<small>
  <div id="mensagem">

  </div>
</small> 

</div>



<div class="modal-footer">


<input value="<?php echo @$_GET['id'] ?>" type="hidden" name="txtid2" id="txtid2">

  <button type="button" id="btn-fechar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  <button type="submit" name="btn-salvar" id="btn-salvar" class="btn btn-primary">Salvar</button>
</div>
</form>
</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php 
        if (@$_GET['funcao'] == 'editar') {
          $titulo = "Editar Banner";
          $id2 = $_GET['id'];

          $query = $pdo->query("SELECT * FROM banners_topo where id = '" . $id2 . "' ");
          $res = $query->fetchAll(PDO::FETCH_ASSOC);

          $nome_banner = $res[0]['nome_banner'];
         
          $texto_acao_banner = $res[0]['texto_acao_banner'];
          $titulo_banner = $res[0]['titulo_banner'];
          $link2 = $res[0]['link'];
          $order_banner = $res[0]['order_banner'];
          $imagem2 = $res[0]['imagem'];
          $id_banner = $res[0]['id'];
          $ativo2 = $res[0]['ativo'];
          
        } 


        ?>

        <h5 class="modal-title" id="exampleModalLabel"><?php echo $titulo ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="formEditar" method="POST">
        <div class="modal-body">

            <div class="row">
              
                <div class="col-md-11">
                    <div class="form-group">
                        <label >Nome do banner</label>
                        <input value="<?php echo @$nome_banner ?>" type="text" class="form-control form-control-sm" id="nome_banner" name="nome_banner" placeholder="Nome do banner">
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label >Titulo</label>
                       
                        <textarea maxlength="500" class="form-control form-control-sm" id="titulo_banner" name="titulo_banner" ><?php echo @$titulo_banner ?></textarea>
                    </div>
                </div>
          
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label >Texto Chamativo</label>
                       
                        <textarea maxlength="500" class="form-control form-control-sm" id="texto_acao_banner" name="texto_acao_banner" ><?php echo @$texto_acao_banner ?></textarea>
                    </div>
                </div>
          
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Link do produto</label>
                        <input value="<?php echo @$link2 ?>" type="text" class="form-control form-control-sm" id="link" name="link" placeholder="Ex: https://www.idealmagazine/produto-vestidos">
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Ordem do banner</label>
                        <input value="<?php echo @$order_banner ?>" type="text" class="form-control form-control-sm" id="order_banner" name="order_banner" placeholder="Ordem do banner">
                    </div>
                    
                </div>
            </div>

            


      <div class="row">
      

        <div class="col-md-4">
          <div class="form-group">
            <label >Imagem do banner</label>
            <input type="file" value="<?php echo @$imagem2 ?>"  class="form-control-file" id="imagem" name="imagem" onChange="carregarImg();">
          </div>

          <?php if(@$imagem2 != ""){ ?>
            <img src="../../img/banner/home/<?php echo $imagem2 ?>" width="200" height="200" id="target">
           
          <?php  }else{ ?>
            <img src="../../img/produtos/sem-foto.jpg" width="100" height="100" id="target">
            <?php } ?>
        </div>
        <div class="col-md-3">

          <div class="form-group">
            <label >Ativo</label>
            <select class="form-control form-control-sm" name="ativo" id="ativo">
              <?php 
              if (@$_GET['funcao'] == 'editar') {

                echo "<option value='".$ativo2."' >" . $ativo2 . "</option>";
              }


              if(@$ativo2 != "Sim"){
                echo "<option value='Sim'>Sim</option>"; 
              }

              if(@$ativo2 != "Não"){
                echo "<option value='Não'>Não</option>"; 
              }

              ?>
            </select>               
          </div>
          </div>
      </div>
      






<small>
  <div id="mensagem">

  </div>
</small> 

</div>



<div class="modal-footer">


<input value="<?php echo @$_GET['id'] ?>" type="hidden" name="txtid2" id="txtid2">

  <button type="button" id="btn-fechar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  <button type="submit" name="btn-salvar" id="btn-salvar" class="btn btn-primary">Salvar</button>
</div>
</form>
</div>
</div>
</div>




<div class="modal" id="modal-deletar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Excluir Banner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <p>Deseja realmente Excluir este Banner?</p>

        <div align="center" id="mensagem_excluir" class="">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-cancelar-excluir">Cancelar</button>
        <form method="post">

          <input type="hidden" id="id"  name="id" value="<?php echo @$_GET['id'] ?>" required>

          <button type="button" id="btn-deletar" name="btn-deletar" class="btn btn-danger">Excluir</button>
        </form>
      </div>
    </div>
  </div>
</div>






<div class="modal" id="modal-imagens" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Imagens do Produto</h5>
        <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-fotos" method="POST" enctype="multipart/form-data" >
          <div class="row">
            <div class="col-md-5">
              <div class="col-md-12 form-group">
                <label>Imagem do Produto</label>
                <input type="file" class="form-control-file" id="imgproduto" name="imgproduto" onchange="carregarImgProduto();">

              </div>

              <div class="col-md-12 mb-2">
                <img src="../../img/produtos/detalhes/sem-foto.jpg" alt="Carregue sua Imagem" id="targetImgProduto" width="100%">
              </div>

            </div>

            <div class="col-md-7" id="listar-img">

            </div>




          </div>

          <div class="col-md-12" align="right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-cancelar-fotos">Cancelar</button>

            <input type="hidden" id="id"  name="id" value="<?php echo @$_GET['id'] ?>">

            <button type="submit" id="btn-fotos" name="btn-fotos" class="btn btn-info">Salvar</button>

          </div>


          <small>     
            <div align="center" id="mensagem_fotos" class="">

            </div>
          </small>   
        </form>
      </div>

    </div>
  </div>
</div>   







<div class="modal" id="modalDeletarImg" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Excluir Imagem</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <p>Deseja realmente Excluir esta Imagem?</p>

        <div align="center" id="mensagem_excluir_img" class="">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn-cancelar-img">Cancelar</button>
        <form method="post">
          <input type="hidden" name="id_foto" id="id_foto">                  
          <button type="button" id="btn-deletar-img" name="btn-deletar-img" class="btn btn-danger">Excluir</button>
        </form>
      </div>
    </div>
  </div>
</div>












<?php 

if (@$_GET["funcao"] != null && @$_GET["funcao"] == "novo") {
  echo "<script>$('#modalDados').modal('show');</script>";
}

if (@$_GET["funcao"] != null && @$_GET["funcao"] == "editar") {
  echo "<script>$('#modalEditar').modal('show');</script>";
}

if (@$_GET["funcao"] != null && @$_GET["funcao"] == "excluir") {
  echo "<script>$('#modal-deletar').modal('show');</script>";
}

if (@$_GET["funcao"] != null && @$_GET["funcao"] == "imagens") {
  echo "<script>$('#modal-imagens').modal('show');</script>";
}


if (@$_GET["funcao"] != null && @$_GET["funcao"] == "carac") {
  echo "<script>$('#modal-carac').modal('show'); </script>";
}


if (@$_GET["funcao"] != null && @$_GET["funcao"] == "promocao") {
  echo "<script>$('#modalPromocao').modal('show'); </script>";
}


?>




<!--AJAX PARA INSERÇÃO  DOS DADOS COM IMAGEM -->
<script type="text/javascript">
  $("#form").submit(function () {
    var pag = "<?=$pag?>";
    event.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      url: pag + "/inserir.php",
      type: 'POST',
      data: formData,

      success: function (mensagem) {

        $('#mensagem').removeClass()

        if (mensagem.trim() == "Salvo com Sucesso!!" || mensagem.trim() == "Editado com sucesso!!!") {

                    //$('#nome').val('');
                    //$('#cpf').val('');
                    $('#btn-fechar').click();
                    window.location = "index.php?pag="+pag;

                  } else {

                    $('#mensagem').addClass('text-danger')
                  }

                  $('#mensagem').text(mensagem)

                },

                cache: false,
                contentType: false,
                processData: false,
            xhr: function () {  // Custom XMLHttpRequest
              var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                  myXhr.upload.addEventListener('progress', function () {
                    /* faz alguma coisa durante o progresso do upload */
                  }, false);
                }
                return myXhr;
              }
            });
  });
</script>

<!--AJAX PARA INSERÇÃO  DOS DADOS COM IMAGEM -->
<script type="text/javascript">
  $("#formEditar").submit(function () {
    var pag = "<?=$pag?>";
    event.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      url: pag + "/editar.php",
      type: 'POST',
      data: formData,

      success: function (mensagem) {

        $('#mensagem').removeClass()
        console.log(mensagem);
        if (mensagem.trim() == "Editado com Sucesso!!") {

                    $('#btn-fechar').click();
                    window.location = "index.php?pag="+pag;

                  } else {

                    $('#mensagem').addClass('text-danger')
                  }

                  $('#mensagem').text(mensagem)

                },

                cache: false,
                contentType: false,
                processData: false,
            xhr: function () {  // Custom XMLHttpRequest
              var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                  myXhr.upload.addEventListener('progress', function () {
                    /* faz alguma coisa durante o progresso do upload */
                  }, false);
                }
                return myXhr;
              }
            });
  });
</script>




<!--AJAX PARA EXCLUSÃO DOS DADOS -->
<script type="text/javascript">
  $(document).ready(function () {
    var pag = "<?=$pag?>";
    $('#btn-deletar').click(function (event) {
      event.preventDefault();

      $.ajax({
        url: pag + "/excluir.php",
        method: "post",
        data: $('form').serialize(),
        dataType: "text",
        success: function (mensagem) {

          if (mensagem.trim() === 'Excluído com Sucesso!!') {


            $('#btn-cancelar-excluir').click();
            window.location = "index.php?pag=" + pag;
          }

          $('#mensagem_excluir').text(mensagem)



        },

      })
    })
  })
</script>







<!--AJAX PARA INSERÇÃO E EDIÇÃO DOS DADOS COM IMAGEM -->
<script type="text/javascript">
  $("#form-fotos").submit(function () {
    var pag = "<?=$pag?>";
    event.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      url: pag + "/inserir-imagens.php",
      type: 'POST',
      data: formData,

      success: function (mensagem) {

        $('#mensagem').removeClass()

        if (mensagem.trim() == "Salvo com Sucesso!!") {

         $('#mensagem_fotos').addClass('text-success')
         $('#mensagem_fotos').text(mensagem)
         listarImagensProd();

       } else {

        $('#mensagem_fotos').addClass('text-danger')
      }

      $('#mensagem_fotos').text(mensagem)

    },

    cache: false,
    contentType: false,
    processData: false,
            xhr: function () {  // Custom XMLHttpRequest
              var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Avalia se tem suporte a propriedade upload
                  myXhr.upload.addEventListener('progress', function () {
                    /* faz alguma coisa durante o progresso do upload */
                  }, false);
                }
                return myXhr;
              }
            });
  });
</script>




<script type="text/javascript">
  function listarImagensProd() {
    var pag = "<?=$pag?>";
    $.ajax({
      url: pag + "/listar-imagens.php",
      method: "post",
      data: $('form').serialize(),
      dataType: "html",
      success: function (result) {

        $('#listar-img').html(result);
      }
    })
  }
</script>




<script type="text/javascript">
  function listarCarac() {
    var pag = "<?=$pag?>";

    $.ajax({
      url: pag + "/listar-carac.php",
      method: "post",
      data: $('#form-carac').serialize(),
      dataType: "html",
      success: function (result) {

        $('#listar-carac').html(result);
      }
    })
  }
</script>



<script type="text/javascript">
  $('#btn-item-listar').click(function(event){
    event.preventDefault();
    var pag = "<?=$pag?>";

    $.ajax({
      url: pag + "/listar-itens.php",
      method: "post",
      data: $('#form-item-listar').serialize(),
      dataType: "html",
      success: function (result) {

        $('#listar-itens').html(result);
      }
    })
  })

</script>



<!--FUNCAO PARA CHAMAR MODAL DE DELETAR IMAGEM DAS FOTOS -->
<script type="text/javascript">
  function deletarImg(img) {

    document.getElementById('id_foto').value = img;
    $('#modalDeletarImg').modal('show');

  }
</script>


<!--FUNCAO PARA CHAMAR MODAL DE DELETAR CARAC DOS PRODUTOS -->
<script type="text/javascript">
  function deletarCarac(id) {

    document.getElementById('id_carac').value = id;
    $('#modalDeletarCarac').modal('show');

  }
</script>



<!--FUNCAO PARA CHAMAR MODAL DE DELETAR ITEM DA CARAC -->
<script type="text/javascript">
  function deletarItem(id) {

    document.getElementById('id_item_carac').value = id;
    $('#modalDeletarItem').modal('show');

  }
</script>



<!--FUNCAO PARA CHAMAR MODAL DE ADD ITEM A CARAC -->
<script type="text/javascript">
  function addItem(id) {

    document.getElementById('id_carac_item').value = id;
    document.getElementById('id_carac_item_2').value = id;
    $('#btn-item-listar').click();
    $('#modalAddItem').modal('show');

  }
</script>




<!--AJAX PARA EXCLUSÃO DAS IMAGENS -->
<script type="text/javascript">
  $(document).ready(function () {
    var pag = "<?=$pag?>";
    $('#btn-deletar-img').click(function (event) {
      event.preventDefault();

      $.ajax({
        url: pag + "/excluir-imagem.php",
        method: "post",
        data: $('form').serialize(),
        dataType: "text",
        success: function (mensagem) {

          if (mensagem.trim() === 'Excluído com Sucesso!!') {


            $('#mensagem_fotos').text(mensagem)
            $('#btn-cancelar-img').click();
            listarImagensProd();
          }

          $('#mensagem_excluir_img').text(mensagem)



        },

      })
    })
  })
</script>





<!--AJAX PARA EXCLUSÃO DAS CARAC -->
<script type="text/javascript">
  $(document).ready(function () {
    var pag = "<?=$pag?>";
    $('#btn-deletar-carac').click(function (event) {
      event.preventDefault();

      $.ajax({
        url: pag + "/excluir-carac.php",
        method: "post",
        data: $('form').serialize(),
        dataType: "text",
        success: function (mensagem) {

          if (mensagem.trim() === 'Excluído com Sucesso!!') {

            $('#btn-cancelar-carac-deletar').click();
            $('#mensagem_carac').text(mensagem)

            listarCarac();
          }

          $('#mensagem_excluir_carac').text(mensagem)



        },

      })
    })
  })
</script>





<!--AJAX PARA EXCLUSÃO DOS ITENS -->
<script type="text/javascript">
  $(document).ready(function () {
    var pag = "<?=$pag?>";
    $('#btn-deletar-item').click(function (event) {

      event.preventDefault();

      $.ajax({
        url: pag + "/excluir-item.php",
        method: "post",
        data: $('form').serialize(),
        dataType: "text",
        success: function (mensagem) {

          if (mensagem.trim() === 'Excluído com Sucesso!!') {

            $('#btn-cancelar-item-deletar').click();
            $('#mensagem_item').text(mensagem)

            $('#btn-item-listar').click();
          }

          $('#mensagem_excluir_item').text(mensagem)



        },

      })
    })
  })
</script>



<!--SCRIPT QUE É EXECUTADO AO CARREGAR A PÁGINA -->
<script type="text/javascript">
  
</script>

<script type="text/javascript">
  function listarSubCat() {
    var pag = "<?=$pag?>";
    $.ajax({
      url: pag + "/listar-subcat.php",
      method: "post",
      data: $('form').serialize(),
      dataType: "html",
      success: function (result) {

        $('#listar-subcat').html(result);
      }
    })
  }
</script>


<!-- Script para buscar pelo select -->
<script type="text/javascript">

  $('#categoria').change(function () {
    document.getElementById('txtCat').value = $(this).val();
    document.getElementById('txtSub').value = "";
    listarSubCat();


  })

</script>





<!--SCRIPT PARA CARREGAR IMAGEM PRINCIPAL -->
<script type="text/javascript">

  function carregarImg() {

    var target = document.getElementById('target');
    var file = document.querySelector("input[type=file]").files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
      target.src = reader.result;
      console.log( "Resultado da imagem" .target.src);
    };
     
    

    if (file) {
      reader.readAsDataURL(file);
      console.log( "Resultado da imagem" .reader);

    } else {
      target.src = "";
    }
  }

</script>




<!--SCRIPT PARA CARREGAR IMAGENS DO PRODUTO -->
<script type="text/javascript">

  function carregarImgProduto() {

    var target = document.getElementById('targetImgProduto');
    var file = document.querySelector("input[id=imgproduto]").files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
      target.src = reader.result;
    };

    if (file) {
      reader.readAsDataURL(file);


    } else {
      target.src = "";
    }
  }

</script>






<script type="text/javascript">
  $('#btn-add-carac').click(function(event){
    event.preventDefault();
    var pag = "<?=$pag?>";
    $.ajax({
      url: pag + "/add-carac.php",
      method:"post",
      data: $('#form-carac').serialize(),
      dataType: "text",
      success: function(msg){
        if(msg.trim() === 'Salvo com Sucesso!!'){

          $('#mensagem_carac').addClass('text-success')
          $('#mensagem_carac').text(msg);
          listarCarac();

        }
        else{
          $('#mensagem_carac').addClass('text-danger')
          $('#mensagem_carac').text(msg);


        }
      }
    })
  })
</script>




<script type="text/javascript">
  $('#btn-item').click(function(event){
    event.preventDefault();
    var pag = "<?=$pag?>";
    $.ajax({
      url: pag + "/add-item.php",
      method:"post",
      data: $('#form-item').serialize(),
      dataType: "text",
      success: function(msg){
        if(msg.trim() === 'Salvo com Sucesso!!'){

          $('#mensagem_item').addClass('text-success')
          $('#mensagem_item').text(msg);
          $('#btn-item-listar').click();
          document.getElementById('nome-item').value = "";
          document.getElementById('nome-item').focus();
        }
        else{
          $('#mensagem_item').addClass('text-danger')
          $('#mensagem_item').text(msg);


        }
      }
    })
  })
</script>






<script type="text/javascript">
  $('#btn-promocao').click(function(event){
    event.preventDefault();
    var pag = "<?=$pag?>";
    $.ajax({
      url: pag + "/add-promocao.php",
      method:"post",
      data: $('#form-promocao').serialize(),
      dataType: "text",
      success: function(msg){
        if(msg.trim() === 'Salvo com Sucesso!!'){


          $('#btn-cancelar-promocao').click();
          window.location = "index.php?pag="+pag;
        }
        else{
          $('#mensagem_promocao').addClass('text-danger')
          $('#mensagem_promocao').text(msg);


        }
      }
    })
  })
</script>





<script type="text/javascript">
  $(document).ready(function () {
    $('#dataTable').dataTable({
      "ordering": false
    })

  });
</script>





